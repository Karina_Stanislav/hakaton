package com.karina.ru.test.di.module;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.karina.ru.test.di.qualifier.ViewHolderContext;
import com.karina.ru.test.di.scope.PerViewHolder;

import dagger.Module;
import dagger.Provides;

@Module
public class ViewHolderModule {

    private RecyclerView.ViewHolder mViewHolder;
    private View mItemView;

    public ViewHolderModule(RecyclerView.ViewHolder viewHolder, View itemView){
        this.mViewHolder = viewHolder;
        this.mItemView = itemView;
    }

    @Provides
    @PerViewHolder
    RecyclerView.ViewHolder provideViewHolder(){
        return mViewHolder;
    }

    @Provides
    @PerViewHolder
    View provideItemView(){
        return mItemView;
    }

    @Provides
    @PerViewHolder
    @ViewHolderContext
    Context provideContext(){
        return mItemView.getContext();
    }

}
