package com.karina.ru.test.main.step2.view;

import android.os.Bundle;

import com.karina.ru.test.R;
import com.karina.ru.test.activity.BaseBackContainerActivity;
import com.karina.ru.test.main.step1.view.Step1Fragment;

public class Step2Activity extends BaseBackContainerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.step2_title);
        openFragment();
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.content, getScreenCreator().newInstance(Step2Fragment.class)).commitAllowingStateLoss();
    }
}
