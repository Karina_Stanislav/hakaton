package com.karina.ru.test;

import com.karina.ru.test.rest.RequestField;

/**
 * Created by Karina on 03.06.2019.
 */
public class Urls {

    public static final String BASE_URL = "http://tomcat.rs-soft.site/hackathon-app-1.0-SNAPSHOT/";

    public static final String CLIENT_FIND = "dictionaries/clients/find/";
    public static final String CLIENT_CREATE = "dictionaries/clients/create/";
    public static final String REGIONS = "dictionaries/regions/";
    public static final String CITIES = "dictionaries/regions/{" + RequestField.REGION_ID + "}/cities/";
    public static final String POLYCLINICS = "dictionaries/cities/{" + RequestField.CITY_ID + "}/polyclinics/";

}
