package com.karina.ru.test.mvp;

import com.karina.ru.test.App;
import com.karina.ru.test.eventbus.EventBusController;
import com.karina.ru.test.eventbus.IEventBusObserver;
import com.karina.ru.test.eventbus.IEventObserver;
import com.karina.ru.test.managers.RequestManager;
import com.karina.ru.test.request.RequestController;

import javax.inject.Inject;

public abstract class BaseEventBusPresenter<T extends IBaseMvpView> extends BasePresenter<T> implements IEventBusObserver {

    @Inject
    protected EventBusController mEventBusController;

    private RequestManager mRequestManager;

    @Override
    public void attachView(T mvpView) {
        super.attachView(mvpView);
        mEventBusController.addObserver(this);
    }

    @Override
    public void detachView() {
        super.detachView();
        mEventBusController.removeObserver(this);
    }


    protected RequestManager getRequestManager() {
        if (mRequestManager == null) {
            mRequestManager = App.mInstance.getComponent().requestmanager();
        }
        return mRequestManager;
    }


    protected RequestController getRequestController(int actionCode) {
        return new RequestController(mEventBusController, actionCode, getMvpView().getClassUniqueDeviceId());
    }

}
