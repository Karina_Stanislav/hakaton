package com.karina.ru.test;

/**
 * Created by Karina on 03.06.2019.
 */
public class Actions {

    public static final int GET_REGIONS = 1000;
    public static final int GET_CITIES = 1002;
    public static final int GET_POLYCLINICS = 1003;
    public static final int CREATE_CLIENT = 1004;
    public static final int GET_CLIENT = 1005;

}
