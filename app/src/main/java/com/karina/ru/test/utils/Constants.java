package com.karina.ru.test.utils;


public class Constants {


    public static final int START_SELECTED_ACTIVITY = 101;
    public static final int START_STEP1_ACTIVITY = 102;
    public static final int START_STEP2_ACTIVITY = 103;
    public static final int START_STEP3_ACTIVITY = 104;
    public static final int START_STEP4_ACTIVITY = 105;




    public static final int RESULT_SELECTED_ACTIVITY = 1001;
    public static final int RESULT_STEP1_ACTIVITY = 1002;


    public static final String SELECTED_ITEM = "SELECTED_ITEM";


    public static final int REGION = 0;
    public static final int CITY = 1;
    public static final int ORGANIZATION = 3;
    public static final int DIAGNOZ = 4;
    public static final int WayToWrite = 5;
    public static final int Time = 6;
    public static final int Specialist = 7;

    public static final String RUR = "RUR";
    public static final String RUB = "RUB";
    public static final String USD = "USD";
    public static final String EUR = "EUR";
    public static final String CHF = "CHF";
    public static final String GBP = "GBP";
    public static final String CNY = "CNY";
}
