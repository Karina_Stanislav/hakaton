package com.karina.ru.test.request;

/**
 * Created by Karina on 05.06.2019.
 */
public interface IRequestCallbackNew {

    void onStartRequest();

    void onFinishRequest();

    void onErrorRequest(String message);

    void onSuccess(Object data);
}
