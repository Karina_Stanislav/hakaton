package com.karina.ru.test.main.main.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.karina.ru.test.R;
import com.karina.ru.test.rest.response.DataResponse;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DataResponse> dataList = new ArrayList<>();

    public void setDataList(List<DataResponse> dataList) {
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder = null;
        return new ViewHolderItem(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_expanded, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ViewHolderItem viewHolderItem = (ViewHolderItem) viewHolder;
        if (i==0)
        viewHolderItem.tvMainRateTitle.setText("ГБУЗ РК Симферопольская поликлиника № 4");
        if (i == 1) {
            viewHolderItem.tvMainRateTitle.setText("ГБУЗ РК Поликлиника № 3");
            viewHolderItem.idMainRate.setRating(4);
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class ViewHolderItem extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_main_rate_title)
        TextView tvMainRateTitle;
        @BindView(R.id.iv_arrow)
        ImageView ivArrow;
        @BindView(R.id.id_main_rate)
        RatingBar idMainRate;
        @BindView(R.id.el_rate)
        ExpandableLayout elRate;

        public ViewHolderItem(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (elRate.isExpanded()) {
                        elRate.collapse();
                        elRate.setVisibility(View.VISIBLE);
                        ivArrow.setImageResource(R.drawable.ic_arrow_down);
                    } else {
                        elRate.expand();
                        elRate.setVisibility(View.VISIBLE);
                        ivArrow.setImageResource(R.drawable.ic_arrow_up);
                    }
                }
            });
        }
    }
}
