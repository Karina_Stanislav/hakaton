package com.karina.ru.test.dataclases;


import android.os.Parcel;
import android.os.Parcelable;

public class StepOne implements Parcelable {

    private Long diagnosisId;

    private String symptoms;

    private Long polyclinicId;

    public Long getDiagnosisId() {
        return diagnosisId;
    }

    public void setDiagnosisId(Long diagnosisId) {
        this.diagnosisId = diagnosisId;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public Long getPolyclinicId() {
        return polyclinicId;
    }

    public void setPolyclinicId(Long polyclinicId) {
        this.polyclinicId = polyclinicId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.diagnosisId);
        dest.writeString(this.symptoms);
        dest.writeValue(this.polyclinicId);
    }

    public StepOne() {
    }

    protected StepOne(Parcel in) {
        this.diagnosisId = (Long) in.readValue(Long.class.getClassLoader());
        this.symptoms = in.readString();
        this.polyclinicId = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Parcelable.Creator<StepOne> CREATOR = new Parcelable.Creator<StepOne>() {
        @Override
        public StepOne createFromParcel(Parcel source) {
            return new StepOne(source);
        }

        @Override
        public StepOne[] newArray(int size) {
            return new StepOne[size];
        }
    };
}
