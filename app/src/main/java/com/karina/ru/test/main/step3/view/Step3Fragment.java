package com.karina.ru.test.main.step3.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.karina.ru.test.R;
import com.karina.ru.test.di.components.FragmentComponent;
import com.karina.ru.test.fragment.BaseMvpFragment;
import com.karina.ru.test.main.selected.view.SelectedActivity;
import com.karina.ru.test.main.step2.view.Step2Fragment;
import com.karina.ru.test.main.step3.IStep3Contract;
import com.karina.ru.test.main.step4.view.Step4Activity;
import com.karina.ru.test.utils.Constants;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class Step3Fragment extends BaseMvpFragment implements IStep3Contract.View {

    @Inject
    IStep3Contract.Presenter mPresenter;

    @BindView(R.id.tv_way_to_write)
    TextView tvWayToWrite;
    @BindView(R.id.et_ticket_number)
    TextView etTicketNumber;
    @BindView(R.id.cb_temperature)
    CheckBox cbTemperature;
    @BindView(R.id.cb_blood_pressure)
    CheckBox cbBloodPressure;
    @BindView(R.id.cb_inspection)
    CheckBox cbInspection;
    @BindView(R.id.btn_next)
    Button btnNext;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.onCreate(savedInstanceState);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getScreenCreator().startActivity(mActivity, Step4Activity.class, Constants.START_STEP1_ACTIVITY);
            }
        });
        tvWayToWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(SelectedActivity.FILTER_DATA, Constants.Time);
                getScreenCreator().startActivity(Step3Fragment.this, mActivity, SelectedActivity.class, bundle, Constants.START_SELECTED_ACTIVITY);
            }
        });
        etTicketNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(SelectedActivity.FILTER_DATA, Constants.DIAGNOZ);
                getScreenCreator().startActivity(Step3Fragment.this, mActivity, SelectedActivity.class, bundle, Constants.START_SELECTED_ACTIVITY);
            }
        });
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_step3;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.START_SELECTED_ACTIVITY:
                if (resultCode == Constants.RESULT_SELECTED_ACTIVITY) {
                    if (data.getExtras() != null) {
                        mPresenter.setSelectedItem(data.getExtras().getParcelable(Constants.SELECTED_ITEM), data.getExtras().getInt(SelectedActivity.FILTER_DATA));
                    }
                }
                break;
        }
    }

    @Override
    public void showWayToWrite(String name) {
        tvWayToWrite.setText(name);
    }

    @Override
    public void showTime(String name) {
        etTicketNumber.setText(name);
    }


}
