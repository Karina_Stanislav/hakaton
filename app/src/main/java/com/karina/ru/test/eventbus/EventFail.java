package com.karina.ru.test.eventbus;

/**
 * Created by Karina on 03.06.2019.
 */
public class EventFail extends Event {

    private String message;

    public EventFail(int actionCode, int mClassUniqueId, String message) {
        super(actionCode, mClassUniqueId, EventType.FAIL_REQUEST);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
