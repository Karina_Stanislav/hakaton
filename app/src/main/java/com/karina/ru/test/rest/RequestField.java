package com.karina.ru.test.rest;

public class RequestField {

    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String REGION_ID = "regionId";
    public static final String CITY_ID = "cityId";
    public static final String OMS = "oms";
    public static final String FULL_NAME = "fullName";
    public static final String POLYCLINIC_ID = "polyclinicId";
    public static final String EMAIL = "email";

}
