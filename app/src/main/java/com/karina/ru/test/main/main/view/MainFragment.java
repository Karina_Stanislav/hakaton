package com.karina.ru.test.main.main.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.karina.ru.test.R;
import com.karina.ru.test.di.components.FragmentComponent;
import com.karina.ru.test.fragment.BaseMvpFragment;
import com.karina.ru.test.main.main.IMainContract;
import com.karina.ru.test.main.main.adapter.MainAdapter;
import com.karina.ru.test.main.selected.adapter.SelectedAdapter;
import com.sysdata.widget.accordion.FancyAccordionView;

import javax.inject.Inject;

import butterknife.BindView;

public class MainFragment extends BaseMvpFragment implements IMainContract.View{

    @BindView(R.id.rv_list)
    RecyclerView rvList;

    @Inject
    IMainContract.Presenter mPresenter;

    private MainAdapter mMainAdapter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.onCreate(savedInstanceState);
        initAdapters();
    }

    private void initAdapters() {
        mMainAdapter = new MainAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        rvList.setLayoutManager(layoutManager);
        rvList.setAdapter(mMainAdapter);
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_main;
    }

}
