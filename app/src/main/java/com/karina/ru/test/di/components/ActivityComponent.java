package com.karina.ru.test.di.components;

import com.karina.ru.test.di.module.ActivityModule;
import com.karina.ru.test.di.module.PresenterModule;
import com.karina.ru.test.di.scope.PerActivity;

import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = {ActivityModule.class, PresenterModule.class})
public interface ActivityComponent {

}
