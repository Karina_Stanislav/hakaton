package com.karina.ru.test.main.selected.view;

import android.os.Bundle;

import com.karina.ru.test.R;
import com.karina.ru.test.activity.BaseBackContainerActivity;
import com.karina.ru.test.utils.Constants;
import com.karina.ru.test.utils.FieldConverter;

/**
 * Created by Karina on 29.05.2019.
 */
public class SelectedActivity extends BaseBackContainerActivity {

    public static final String FILTER_DATA = "SelectedActivity.FILTER_DATA";
    public static final String ID = "SelectedActivity.ID";

    private Integer filterMode;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getExtras();
        setTitle();
        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState);
        } else {
            openFragment();
        }
    }

    private void getExtras() {
        if (getIntent().getExtras() != null) {
            filterMode = getIntent().getIntExtra(FILTER_DATA, 3);
            id = getIntent().getStringExtra(ID);
        }
    }

    private void setTitle() {
        if (filterMode != null) {
            switch (filterMode) {
                case Constants.REGION:
                    setTitle(FieldConverter.getString(R.string.selected_region));
                    break;
                case Constants.CITY:
                    setTitle(FieldConverter.getString(R.string.selected_city));
                    break;
                case Constants.ORGANIZATION:
                    setTitle(FieldConverter.getString(R.string.selected_organization));
                    break;
                case Constants.DIAGNOZ:
                    setTitle(FieldConverter.getString(R.string.step1_diagnosis));
                    break;
                case Constants.WayToWrite:
                    setTitle(FieldConverter.getString(R.string.step1_diagnosis));
                    break;
                case Constants.Time:
                    setTitle(FieldConverter.getString(R.string.step1_diagnosis));
                    break;
                case Constants.Specialist:
                    setTitle(FieldConverter.getString(R.string.step1_diagnosis));
                    break;
            }
        }
    }

    private Bundle createBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt(SelectedFragment.FILTER_DATA, filterMode);
        bundle.putString(SelectedFragment.ID, id);
        return bundle;
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.content, getScreenCreator().newInstance(SelectedFragment.class, createBundle())).commitAllowingStateLoss();
    }

}
