package com.karina.ru.test.di.components;

import com.karina.ru.test.di.module.PresenterModule;
import com.karina.ru.test.di.module.ServiceModule;
import com.karina.ru.test.di.scope.PerService;

import dagger.Subcomponent;
@PerService
@Subcomponent(modules = {ServiceModule.class, PresenterModule.class})
public interface ServiceComponent {

}
