package com.karina.ru.test.main.step2.presenter;

import android.os.Bundle;

import com.karina.ru.test.eventbus.Event;
import com.karina.ru.test.main.step2.IStep2Contract;
import com.karina.ru.test.mvp.BaseEventBusPresenter;
import com.karina.ru.test.rest.response.DataResponse;
import com.karina.ru.test.utils.Constants;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;

public class Step2Presenter extends BaseEventBusPresenter<IStep2Contract.View>
        implements IStep2Contract.Presenter {

    private Disposable mDisposable;

    @Inject
    public Step2Presenter() {
        mDisposable = Disposables.empty();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {

        }
    }

    @Override
    public void setSelectedItem(DataResponse item, int filterMode) {
        switch (filterMode) {
            case Constants.WayToWrite:
                getMvpView().showWayToWrite(item.getName());
                break;
            case Constants.Time:
                getMvpView().showTime(item.getName());
                break;
            case Constants.Specialist:
                getMvpView().showSpecialist(item.getName());
                break;
        }
    }

    @Override
    public void onStartEvent(Event event) {

    }

    @Override
    public void onFinishEvent(Event event) {

    }

    @Override
    public void onEvent(Event event) {

    }
}
