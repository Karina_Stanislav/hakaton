package com.karina.ru.test.main.step3.view;

import android.os.Bundle;

import com.karina.ru.test.R;
import com.karina.ru.test.activity.BaseBackContainerActivity;

public class Step3Activity extends BaseBackContainerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.step3_title);
        openFragment();
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.content, getScreenCreator().newInstance(Step3Fragment.class)).commitAllowingStateLoss();
    }
}
