package com.karina.ru.test.di.components;

import com.karina.ru.test.di.module.PresenterModule;
import com.karina.ru.test.di.module.ViewHolderModule;
import com.karina.ru.test.di.scope.PerViewHolder;

import dagger.Subcomponent;

@PerViewHolder
@Subcomponent(modules = {ViewHolderModule.class, PresenterModule.class})
public interface ViewHolderComponent {
}
