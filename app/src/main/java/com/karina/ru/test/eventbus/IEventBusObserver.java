package com.karina.ru.test.eventbus;

/**
 * Created by Karina on 03.06.2019.
 */
public interface IEventBusObserver {

    void onStartEvent(Event event);

    void onFinishEvent(Event event);

    void onEvent(Event event);
}
