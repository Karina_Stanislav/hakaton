package com.karina.ru.test.main.selected;

import com.karina.ru.test.mvp.IBaseMvpPresenter;
import com.karina.ru.test.mvp.IBaseMvpView;
import com.karina.ru.test.rest.response.DataResponse;

import java.util.List;

/**
 * Created by Karina on 29.05.2019.
 */
public interface ISelectedContract {

    interface View extends IBaseMvpView {


        void setDataList(List<DataResponse> list);

        void setResultSelectedItem(DataResponse dataResponse, Integer filterMode);
    }

    interface Presenter extends IBaseMvpPresenter<View> {

        void selectedItem(int position);
    }
}
