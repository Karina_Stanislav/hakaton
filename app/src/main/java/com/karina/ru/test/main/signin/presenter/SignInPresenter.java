package com.karina.ru.test.main.signin.presenter;

import android.os.Bundle;

import com.karina.ru.test.Actions;
import com.karina.ru.test.eventbus.Event;
import com.karina.ru.test.eventbus.EventSuccess;
import com.karina.ru.test.main.signin.ISignInContract;
import com.karina.ru.test.mvp.BaseEventBusPresenter;
import com.karina.ru.test.rest.RequestParams;
import com.karina.ru.test.rest.response.ClientResponse;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;

public class SignInPresenter extends BaseEventBusPresenter<ISignInContract.View>
        implements ISignInContract.Presenter {

    private Disposable mDisposable;

    @Inject
    public SignInPresenter() {
        mDisposable = Disposables.empty();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {

        }
    }

    @Override
    public void login(String login, String password) {
        mDisposable = getRequestManager().getClientResponse(RequestParams.getClientParams(login, password),
                getRequestController(Actions.GET_CLIENT));
        addDisposable(mDisposable);
    }

    @Override
    public void onStartEvent(Event event) {
        event.getmEventType();
    }

    @Override
    public void onFinishEvent(Event event) {

    }

    @Override
    public void onEvent(Event event) {
        if (event.getmClassUniqueId() == getMvpView().getClassUniqueDeviceId()) {
            switch (event.getmEventType()) {
                case SUCCESS_REQUEST:
                    switch (event.getActionCode()) {
                        case Actions.GET_CLIENT:
                            ClientResponse dataResponse = (ClientResponse) ((EventSuccess) event).getData();
                            getMvpView().showMainScreen();
                            break;
                    }
                    break;
            }

        }
    }
}
