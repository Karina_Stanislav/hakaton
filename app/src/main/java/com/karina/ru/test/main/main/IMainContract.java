package com.karina.ru.test.main.main;

import com.karina.ru.test.mvp.IBaseMvpPresenter;
import com.karina.ru.test.mvp.IBaseMvpView;

public interface IMainContract {


    interface View extends IBaseMvpView {


    }

    interface Presenter extends IBaseMvpPresenter<View> {


    }
}
