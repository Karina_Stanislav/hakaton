package com.karina.ru.test.eventbus;

/**
 * Created by Karina on 03.06.2019.
 */
public enum EventType {
    CUSTOM_EVENT,
    START_REQUEST,
    FINISH_REQUEST,
    SUCCESS_REQUEST,
    FAIL_REQUEST,
}
