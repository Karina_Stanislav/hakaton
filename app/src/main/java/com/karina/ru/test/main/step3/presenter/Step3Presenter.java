package com.karina.ru.test.main.step3.presenter;

import android.os.Bundle;

import com.karina.ru.test.eventbus.Event;
import com.karina.ru.test.mvp.BaseEventBusPresenter;
import com.karina.ru.test.main.step3.IStep3Contract;
import com.karina.ru.test.rest.response.DataResponse;
import com.karina.ru.test.utils.Constants;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;

public class Step3Presenter extends BaseEventBusPresenter<IStep3Contract.View>
        implements IStep3Contract.Presenter {

    private Disposable mDisposable;

    @Inject
    public Step3Presenter() {
        mDisposable = Disposables.empty();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {

        }
    }

    @Override
    public void onStartEvent(Event event) {

    }

    @Override
    public void onFinishEvent(Event event) {

    }

    @Override
    public void onEvent(Event event) {

    }


    @Override
    public void setSelectedItem(DataResponse item, int filterMode) {
        switch (filterMode) {
            case Constants.Time:
                getMvpView().showWayToWrite(item.getName());
                break;
            case Constants.DIAGNOZ:
                getMvpView().showTime(item.getName());
                break;
        }
    }

}
