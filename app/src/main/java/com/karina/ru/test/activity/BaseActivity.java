package com.karina.ru.test.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.karina.ru.test.screencreator.ScreenCreator;

import butterknife.ButterKnife;
import butterknife.Unbinder;


public abstract class BaseActivity extends AppCompatActivity {

    private Unbinder mUnbinder;

    protected boolean isSavedInstanceStateDone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        mUnbinder = ButterKnife.bind(this);
    }

    protected abstract int getLayout();

    protected void retry() {
    }

    protected ScreenCreator getScreenCreator() {
        return ScreenCreator.getInstance();
    }

    @Override
    public void onResume() {
        super.onResume();
        isSavedInstanceStateDone = false;
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        isSavedInstanceStateDone = true;
    }

    public boolean isSavedInstanceStateDone() {
        return isSavedInstanceStateDone;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }
}