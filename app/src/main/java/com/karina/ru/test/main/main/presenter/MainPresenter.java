package com.karina.ru.test.main.main.presenter;

import android.os.Bundle;

import com.karina.ru.test.eventbus.Event;
import com.karina.ru.test.main.main.IMainContract;
import com.karina.ru.test.mvp.BaseEventBusPresenter;
import com.karina.ru.test.rest.RequestParams;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;

public class MainPresenter extends BaseEventBusPresenter<IMainContract.View>
        implements IMainContract.Presenter {

    private Disposable mDisposable;

    @Inject
    public MainPresenter() {
        mDisposable = Disposables.empty();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {

        }
    }

    @Override
    public void onStartEvent(Event event) {

    }

    @Override
    public void onFinishEvent(Event event) {

    }

    @Override
    public void onEvent(Event event) {

    }
}
