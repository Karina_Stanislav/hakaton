package com.karina.ru.test.main.signup;

import com.karina.ru.test.mvp.IBaseMvpPresenter;
import com.karina.ru.test.mvp.IBaseMvpView;
import com.karina.ru.test.rest.response.DataResponse;

public interface ISignUpContract {

    interface View extends IBaseMvpView {


        void showSelectedScreen(int filterMode, String id);

        void showCity(String item);

        void showRegion(String item);

        void showOrganization(String item);

        void showMainScreen();
    }

    interface Presenter extends IBaseMvpPresenter<View> {

        void setSelectedItem(DataResponse item, int filterMode);

        void openSelectedScreen(int filterMode);

        void signUp(String fio, String oms, String email, String login, String password, String repeatPassword);
    }

}
