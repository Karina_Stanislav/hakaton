package com.karina.ru.test.rest;

import java.util.LinkedHashMap;

public class RequestParams {


    public static LinkedHashMap<String, String> getClientParams(String login, String password) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.LOGIN, login);
        params.put(RequestField.PASSWORD, password);
        return params;
    }

    public static LinkedHashMap<String, String> createClientParams(String fio, String oms, String polyclinicId, String login, String password, String email) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.LOGIN, login);
        params.put(RequestField.PASSWORD, password);
        params.put(RequestField.FULL_NAME, fio);
        params.put(RequestField.OMS, oms);
        params.put(RequestField.POLYCLINIC_ID, polyclinicId);
        params.put(RequestField.EMAIL, email);
        return params;
    }
}
