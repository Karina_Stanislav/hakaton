package com.karina.ru.test.dataclases;


import android.os.Parcel;
import android.os.Parcelable;

public class Steps implements Parcelable {

    private String userId;

    private StepOne stepOne;

    private StepTwo stepTwo;

    private StepThree stepThree;

    private StepFour stepFour;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public StepOne getStepOne() {
        return stepOne;
    }

    public void setStepOne(StepOne stepOne) {
        this.stepOne = stepOne;
    }

    public StepTwo getStepTwo() {
        return stepTwo;
    }

    public void setStepTwo(StepTwo stepTwo) {
        this.stepTwo = stepTwo;
    }

    public StepThree getStepThree() {
        return stepThree;
    }

    public void setStepThree(StepThree stepThree) {
        this.stepThree = stepThree;
    }

    public StepFour getStepFour() {
        return stepFour;
    }

    public void setStepFour(StepFour stepFour) {
        this.stepFour = stepFour;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.userId);
        dest.writeParcelable(this.stepOne, flags);
        dest.writeParcelable(this.stepTwo, flags);
        dest.writeParcelable(this.stepThree, flags);
        dest.writeParcelable(this.stepFour, flags);
    }

    public Steps() {
    }

    protected Steps(Parcel in) {
        this.userId = in.readString();
        this.stepOne = in.readParcelable(StepOne.class.getClassLoader());
        this.stepTwo = in.readParcelable(StepTwo.class.getClassLoader());
        this.stepThree = in.readParcelable(StepThree.class.getClassLoader());
        this.stepFour = in.readParcelable(StepFour.class.getClassLoader());
    }

    public static final Parcelable.Creator<Steps> CREATOR = new Parcelable.Creator<Steps>() {
        @Override
        public Steps createFromParcel(Parcel source) {
            return new Steps(source);
        }

        @Override
        public Steps[] newArray(int size) {
            return new Steps[size];
        }
    };
}
