package com.karina.ru.test.rest.service;


import com.karina.ru.test.Urls;
import com.karina.ru.test.rest.RequestField;
import com.karina.ru.test.rest.response.DataResponse;
import com.karina.ru.test.rest.response.ClientResponse;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by Karina on 03.06.2019.
 */
public interface ITestService {

    @GET(Urls.CLIENT_FIND)
    Single<ClientResponse> getClientResponse(@QueryMap LinkedHashMap<String, String> params);

    @GET(Urls.REGIONS)
    Single<ArrayList<DataResponse>> getRegionsResponse();

    @GET(Urls.CITIES)
    Single<ArrayList<DataResponse>> getCitiesResponse(@Path(RequestField.REGION_ID) String regionId);

    @GET(Urls.POLYCLINICS)
    Single<ArrayList<DataResponse>> getPolyclinicsResponse(@Path(RequestField.CITY_ID) String cityId);

    @PUT(Urls.CLIENT_CREATE)
    Single<Response<ClientResponse>> createClientResponse(@Body LinkedHashMap<String, String> params);

//    @GET(Urls.CLIENT)
//    Single<ClientResponse> getClientResponse(@QueryMap LinkedHashMap<String, String> params);

}
