package com.karina.ru.test.di.components;

import com.karina.ru.test.di.module.ActivityModule;
import com.karina.ru.test.di.module.FragmentModule;
import com.karina.ru.test.di.module.ServiceModule;
import com.karina.ru.test.di.module.ViewHolderModule;

import dagger.Subcomponent;

@Subcomponent
public interface ConfigPersistentComponent {

    ActivityComponent activityComponent(ActivityModule activityModule);

    FragmentComponent fragmentComponent(FragmentModule fragmentModule);

    ServiceComponent serviceComponent(ServiceModule serviceModule);

    ViewHolderComponent viewHolderComponent(ViewHolderModule viewHolderModule);

}
