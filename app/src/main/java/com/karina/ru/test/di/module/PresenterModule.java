package com.karina.ru.test.di.module;

import com.karina.ru.test.main.main.IMainContract;
import com.karina.ru.test.main.main.presenter.MainPresenter;
import com.karina.ru.test.main.selected.ISelectedContract;
import com.karina.ru.test.main.selected.presenter.SelectedPresenter;
import com.karina.ru.test.main.signin.ISignInContract;
import com.karina.ru.test.main.signin.presenter.SignInPresenter;
import com.karina.ru.test.main.signup.ISignUpContract;
import com.karina.ru.test.main.signup.presenter.SignUpPresenter;
import com.karina.ru.test.main.step1.IStep1Contract;
import com.karina.ru.test.main.step1.presenter.Step1Presenter;
import com.karina.ru.test.main.step2.IStep2Contract;
import com.karina.ru.test.main.step2.presenter.Step2Presenter;
import com.karina.ru.test.main.step3.IStep3Contract;
import com.karina.ru.test.main.step3.presenter.Step3Presenter;
import com.karina.ru.test.main.step4.IStep4Contract;
import com.karina.ru.test.main.step4.presenter.Step4Presenter;

import dagger.Binds;
import dagger.Module;


@Module
public abstract class PresenterModule {

    @Binds
    abstract ISignUpContract.Presenter bindSignUpPresenter(SignUpPresenter signUpPresenter);

    @Binds
    abstract ISelectedContract.Presenter bindSelectedPresenter(SelectedPresenter selectedPresenter);

    @Binds
    abstract ISignInContract.Presenter bindSignInPresenter(SignInPresenter signInPresenter);

    @Binds
    abstract IMainContract.Presenter bindMainPresenter(MainPresenter mainPresenter);

    @Binds
    abstract IStep1Contract.Presenter bindStep1Presenter(Step1Presenter step1Presenter);

    @Binds
    abstract IStep2Contract.Presenter bindStep2Presenter(Step2Presenter step1Presenter);

    @Binds
    abstract IStep3Contract.Presenter bindStep3Presenter(Step3Presenter step1Presenter);

    @Binds
    abstract IStep4Contract.Presenter bindStep4Presenter(Step4Presenter step1Presenter);


}

