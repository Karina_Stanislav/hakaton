package com.karina.ru.test.activity;

import android.os.Bundle;
import android.view.View;

import com.karina.ru.test.utils.UI;

public abstract class BaseBackContainerActivity extends BaseContainerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void visibleCancel() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        mIvCancel.setVisibility(View.VISIBLE);
        mIvCancel.setOnClickListener(view -> onBackPressed());
    }

    public void visibleSetting() {
        mIvSetting.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        UI.animationCloseActivity(this);
    }
}
