package com.karina.ru.test.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.karina.ru.test.R;
import com.karina.ru.test.utils.FieldConverter;

import java.util.Objects;


public abstract class BaseContainerActivity extends BaseActivity {

    protected Toolbar mToolbar;
    protected ImageView mIvCancel;
    protected ImageView mIvSetting;
    protected TextView mTitleTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mIvCancel = (ImageView) findViewById(R.id.toolbar_cancel);
        mIvSetting = (ImageView) findViewById(R.id.toolbar_setting);
        mTitleTextView = (TextView) findViewById(R.id.toolbar_title);
        initToolbar();
    }

    @Override
    protected int getLayout() {
        return R.layout.app_bar_main;
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitleTextView.setText(title);
    }

    @Override
    public void setTitle(int titleId) {
        mTitleTextView.setText(FieldConverter.getString(titleId));
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    protected abstract void openFragment();

}
