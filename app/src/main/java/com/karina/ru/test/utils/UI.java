package com.karina.ru.test.utils;

import android.app.Activity;

import com.karina.ru.test.R;

/**
 * Created by j7ars on 25.10.2017.
 */

public class UI {

    public static void animationOpenActivity(Activity activity) {
        activity.overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
    }

    public static void animationCloseActivity(Activity activity) {
        activity.overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
    }


    interface IProductViewCallback {
        void showBalanceDialog();
    }


}
