package com.karina.ru.test.di.components;

import com.karina.ru.test.di.module.ApplicationModule;
import com.karina.ru.test.di.module.NetModule;
import com.karina.ru.test.managers.DataManager;
import com.karina.ru.test.managers.RequestManager;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetModule.class})
public interface ApplicationComponent {

    ConfigPersistentComponent configPersistentComponent();

    DataManager dataManager();

    RequestManager requestmanager();

}
