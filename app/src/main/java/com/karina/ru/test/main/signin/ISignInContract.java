package com.karina.ru.test.main.signin;

import com.karina.ru.test.mvp.IBaseMvpPresenter;
import com.karina.ru.test.mvp.IBaseMvpView;

public interface ISignInContract {
    interface View extends IBaseMvpView {

        void showMainScreen();
    }

    interface Presenter extends IBaseMvpPresenter<View> {

        void login(String login, String password);
    }

}
