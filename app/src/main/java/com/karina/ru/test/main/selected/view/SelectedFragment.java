package com.karina.ru.test.main.selected.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.karina.ru.test.R;
import com.karina.ru.test.di.components.FragmentComponent;
import com.karina.ru.test.fragment.BaseMvpFragment;
import com.karina.ru.test.main.selected.ISelectedContract;
import com.karina.ru.test.main.selected.adapter.SelectedAdapter;
import com.karina.ru.test.rest.response.DataResponse;
import com.karina.ru.test.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by Karina on 29.05.2019.
 */
public class SelectedFragment extends BaseMvpFragment implements ISelectedContract.View, SelectedAdapter.OnItemClickListener {

    public static final String FILTER_DATA = "SelectedFragment.FILTER_DATA";
    public static final String ID = "SelectedFragment.ID";

    @BindView(R.id.rv_list)
    RecyclerView rvSelectList;

    private SelectedAdapter mSelectedAdapter;

    @Inject
    ISelectedContract.Presenter mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgs();
    }

    private void getArgs() {
        if (getArguments() != null) {
            mPresenter.setArguments(getArguments().getInt(FILTER_DATA), getArguments().getString(ID));
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initAdapters();
        mPresenter.onCreate(savedInstanceState);
    }

    private void initAdapters() {
        mSelectedAdapter = new SelectedAdapter();
        mSelectedAdapter.setOnItemClickListener(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        rvSelectList.setLayoutManager(layoutManager);
        rvSelectList.setAdapter(mSelectedAdapter);
    }

    @Override
    public void setDataList(List<DataResponse> list) {
        mSelectedAdapter.setDataList(list);
    }

    @Override
    public void setResultSelectedItem(DataResponse item, Integer filterMode) {
        Intent intent = new Intent();
        intent.putExtra(Constants.SELECTED_ITEM, item);
        intent.putExtra(SelectedActivity.FILTER_DATA, filterMode);
        mActivity.setResult(Constants.RESULT_SELECTED_ACTIVITY, intent);
        mActivity.finish();
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_select_list;
    }

    @Override
    public void onItemClick(int position) {
        mPresenter.selectedItem(position);
    }
}
