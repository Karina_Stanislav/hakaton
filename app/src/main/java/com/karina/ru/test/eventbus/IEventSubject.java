package com.karina.ru.test.eventbus;

public interface IEventSubject {

    void addObserver(final IEventBusObserver iObserver);

    void removeObserver(final IEventBusObserver iObserver);

    void removeAllObservers();

    void notifyEvent(Event event);

    void notifyStartedWithAction(final int action, final int classUniqueId);

    void notifyFinishWithAction(final int action, final int classUniqueId);

    void notifySuccess(final int actionCode, final Object data, final int classUniqueId);

    void notifyFailed(final int action, final int classUniqueId, String message);


}
