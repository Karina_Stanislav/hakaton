package com.karina.ru.test.rest.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class DataResponse implements Parcelable {

    @SerializedName("id")
    private long cityId;
    @SerializedName("name")
    private String regionName;

    public long getId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.cityId);
        dest.writeString(this.regionName);
    }

    public DataResponse() {
    }

    protected DataResponse(Parcel in) {
        this.cityId = in.readLong();
        this.regionName = in.readString();
    }

    public static final Creator<DataResponse> CREATOR = new Creator<DataResponse>() {
        @Override
        public DataResponse createFromParcel(Parcel source) {
            return new DataResponse(source);
        }

        @Override
        public DataResponse[] newArray(int size) {
            return new DataResponse[size];
        }
    };
}
