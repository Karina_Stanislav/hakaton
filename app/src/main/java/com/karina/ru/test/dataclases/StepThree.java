package com.karina.ru.test.dataclases;


import android.os.Parcel;
import android.os.Parcelable;

public class StepThree implements Parcelable {

    private String appointmentDateActual;

    private String medicName;

    private Meta meta;

    public String getAppointmentDateActual() {
        return appointmentDateActual;
    }

    public void setAppointmentDateActual(String appointmentDateActual) {
        this.appointmentDateActual = appointmentDateActual;
    }

    public String getMedicName() {
        return medicName;
    }

    public void setMedicName(String medicName) {
        this.medicName = medicName;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.appointmentDateActual);
        dest.writeString(this.medicName);
        dest.writeParcelable(this.meta, flags);
    }

    public StepThree() {
    }

    protected StepThree(Parcel in) {
        this.appointmentDateActual = in.readString();
        this.medicName = in.readString();
        this.meta = in.readParcelable(Meta.class.getClassLoader());
    }

    public static final Parcelable.Creator<StepThree> CREATOR = new Parcelable.Creator<StepThree>() {
        @Override
        public StepThree createFromParcel(Parcel source) {
            return new StepThree(source);
        }

        @Override
        public StepThree[] newArray(int size) {
            return new StepThree[size];
        }
    };
}
