package com.karina.ru.test.dataclases;


import android.os.Parcel;
import android.os.Parcelable;

public class Meta implements Parcelable {

    private boolean temperature;

    private boolean bloodPressure;

    private boolean inspection;

    public boolean isTemperature() {
        return temperature;
    }

    public void setTemperature(boolean temperature) {
        this.temperature = temperature;
    }

    public boolean isBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(boolean bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public boolean isInspection() {
        return inspection;
    }

    public void setInspection(boolean inspection) {
        this.inspection = inspection;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.temperature ? (byte) 1 : (byte) 0);
        dest.writeByte(this.bloodPressure ? (byte) 1 : (byte) 0);
        dest.writeByte(this.inspection ? (byte) 1 : (byte) 0);
    }

    public Meta() {
    }

    protected Meta(Parcel in) {
        this.temperature = in.readByte() != 0;
        this.bloodPressure = in.readByte() != 0;
        this.inspection = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Meta> CREATOR = new Parcelable.Creator<Meta>() {
        @Override
        public Meta createFromParcel(Parcel source) {
            return new Meta(source);
        }

        @Override
        public Meta[] newArray(int size) {
            return new Meta[size];
        }
    };
}
