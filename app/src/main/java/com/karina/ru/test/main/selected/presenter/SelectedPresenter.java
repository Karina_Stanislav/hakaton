package com.karina.ru.test.main.selected.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.karina.ru.test.Actions;
import com.karina.ru.test.eventbus.Event;
import com.karina.ru.test.eventbus.EventSuccess;
import com.karina.ru.test.main.selected.ISelectedContract;
import com.karina.ru.test.mvp.BaseEventBusPresenter;
import com.karina.ru.test.rest.response.DataResponse;
import com.karina.ru.test.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;

/**
 * Created by Karina on 29.05.2019.
 */
public class SelectedPresenter extends BaseEventBusPresenter<ISelectedContract.View>
        implements ISelectedContract.Presenter {

    public static final String SAVE_FILTER_DATA = "SelectedPresenter.SAVE_FILTER_DATA";

    private Disposable mDisposable;
    private Integer filterMode;
    private String id;
    private List<DataResponse> dataList = new ArrayList<>();

    @Inject
    public SelectedPresenter() {
        mDisposable = Disposables.empty();
    }

    @Override
    public void setArguments(Object... params) {
        if (params != null) {
            filterMode = (Integer) params[0];
            id = (String) params[1];
        }
    }

    @Override
    public void selectedItem(int position) {
        getMvpView().setResultSelectedItem(dataList.get(position), filterMode);
    }

    private void getRegionsList() {
        mDisposable = getRequestManager().getRegionsResponse(getRequestController(Actions.GET_REGIONS));
        addDisposable(mDisposable);
    }

    private void getCitiesList(String id) {
        mDisposable = getRequestManager().getCitiesResponse(id, getRequestController(Actions.GET_CITIES));
        addDisposable(mDisposable);
    }

    private void getPolyclinicsList(String id) {
        mDisposable = getRequestManager().getPolyclinicsResponse(id, getRequestController(Actions.GET_POLYCLINICS));
        addDisposable(mDisposable);
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        outState.putInt(SAVE_FILTER_DATA, filterMode);
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        filterMode = savedInstanceState.getInt(SAVE_FILTER_DATA);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            switch (filterMode) {
                case Constants.REGION:
                    getRegionsList();
                    break;
                case Constants.CITY:
                    getCitiesList(id);
                    break;
                case Constants.ORGANIZATION:
                    if(id == null || id.isEmpty())
                        id = "1";
                    getPolyclinicsList(id);
                    break;
                case Constants.DIAGNOZ:
                    getDiagnoz(id);
                    break;
                case Constants.WayToWrite:
                    getWayToWrite(id);
                    break;
                case Constants.Time:
                    getTime(id);
                    break;
                case Constants.Specialist:
                    getSpecialist(id);
                    break;

            }

        }
    }

    private void getSpecialist(String id) {
        DataResponse dataResponse = new DataResponse();
        dataResponse.setCityId(1);
        dataResponse.setRegionName("Терапевт");
        dataList.add(dataResponse);
        DataResponse dataResponse2 = new DataResponse();
        dataResponse2.setCityId(2);
        dataResponse2.setRegionName("Невролог");
        dataList.add(dataResponse2);
        getMvpView().setDataList(dataList);
    }

    private void getTime(String id) {
        DataResponse dataResponse = new DataResponse();
        dataResponse.setCityId(1);
        dataResponse.setRegionName("12:00");
        dataList.add(dataResponse);
        DataResponse dataResponse2 = new DataResponse();
        dataResponse2.setCityId(2);
        dataResponse2.setRegionName("13:15");
        dataList.add(dataResponse2);
        getMvpView().setDataList(dataList);
    }

    private void getWayToWrite(String id) {
        DataResponse dataResponse = new DataResponse();
        dataResponse.setCityId(1);
        dataResponse.setRegionName("Онлайн");
        dataList.add(dataResponse);
        DataResponse dataResponse2 = new DataResponse();
        dataResponse2.setCityId(2);
        dataResponse2.setRegionName("В регистратуре");
        dataList.add(dataResponse2);
        getMvpView().setDataList(dataList);
    }

    private void getDiagnoz(String id) {
        DataResponse dataResponse = new DataResponse();
        dataResponse.setCityId(1);
        dataResponse.setRegionName("ОРВИ");
        dataList.add(dataResponse);
        DataResponse dataResponse2 = new DataResponse();
        dataResponse2.setCityId(2);
        dataResponse2.setRegionName("ОРЗ");
        dataList.add(dataResponse2);
        getMvpView().setDataList(dataList);
    }

    @Override
    public void onStartEvent(Event event) {

    }

    @Override
    public void onFinishEvent(Event event) {

    }

    @Override
    public void onEvent(Event event) {
        if (event.getmClassUniqueId() == getMvpView().getClassUniqueDeviceId()) {
            switch (event.getmEventType()) {
                case SUCCESS_REQUEST:
                    switch (event.getActionCode()) {
                        case Actions.GET_REGIONS:
                        case Actions.GET_CITIES:
                        case Actions.GET_POLYCLINICS:
                            ArrayList<DataResponse> dataResponse = (ArrayList<DataResponse>) ((EventSuccess) event).getData();
                            dataList.addAll(dataResponse);
                            getMvpView().setDataList(dataList);
                            break;
                    }
                    break;
            }

        }
    }
}