package com.karina.ru.test.managers;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DataManager {

    private DatabaseManager mDatabaseManager;

    @Inject
    public DataManager(DatabaseManager databaseManager) {
        this.mDatabaseManager = databaseManager;
    }

}
