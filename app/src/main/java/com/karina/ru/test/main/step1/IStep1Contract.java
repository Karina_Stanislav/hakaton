package com.karina.ru.test.main.step1;

import android.os.Parcelable;

import com.karina.ru.test.mvp.IBaseMvpPresenter;
import com.karina.ru.test.mvp.IBaseMvpView;
import com.karina.ru.test.rest.response.DataResponse;

public interface IStep1Contract {


    interface View extends IBaseMvpView {


        void showDiagnoz(String name);

        void showOrganization(String name);
    }

    interface Presenter extends IBaseMvpPresenter<View> {


        void setSelectedItem(DataResponse item, int filterMode);
    }
}
