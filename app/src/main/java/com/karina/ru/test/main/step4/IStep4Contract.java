package com.karina.ru.test.main.step4;

import com.karina.ru.test.mvp.IBaseMvpPresenter;
import com.karina.ru.test.mvp.IBaseMvpView;

public interface IStep4Contract {


    interface View extends IBaseMvpView {


    }

    interface Presenter extends IBaseMvpPresenter<View> {


        void saveData();
    }
}
