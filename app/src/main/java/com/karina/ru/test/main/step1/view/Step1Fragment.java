package com.karina.ru.test.main.step1.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.karina.ru.test.R;
import com.karina.ru.test.di.components.FragmentComponent;
import com.karina.ru.test.fragment.BaseMvpFragment;
import com.karina.ru.test.main.selected.view.SelectedActivity;
import com.karina.ru.test.main.step1.IStep1Contract;
import com.karina.ru.test.main.step2.view.Step2Activity;
import com.karina.ru.test.utils.Constants;

import javax.inject.Inject;

import butterknife.BindView;

public class Step1Fragment extends BaseMvpFragment implements IStep1Contract.View {

    @Inject
    IStep1Contract.Presenter mPresenter;

    @BindView(R.id.tv_diagnosis)
    TextView tvDiagnosis;
    @BindView(R.id.et_symptoms)
    EditText etSymptoms;
    @BindView(R.id.tv_organization)
    TextView tvOrganization;
    @BindView(R.id.btn_next)
    Button btnNext;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.onCreate(savedInstanceState);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getScreenCreator().startActivity(mActivity, Step2Activity.class, Constants.START_STEP1_ACTIVITY);
            }
        });
        tvDiagnosis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(SelectedActivity.FILTER_DATA, Constants.DIAGNOZ);
                getScreenCreator().startActivity(Step1Fragment.this, mActivity, SelectedActivity.class, bundle, Constants.START_SELECTED_ACTIVITY);
            }
        });
        tvOrganization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(SelectedActivity.FILTER_DATA, Constants.ORGANIZATION);
                getScreenCreator().startActivity(Step1Fragment.this, mActivity, SelectedActivity.class, bundle, Constants.START_SELECTED_ACTIVITY);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.START_SELECTED_ACTIVITY:
                if (resultCode == Constants.RESULT_SELECTED_ACTIVITY) {
                    if (data.getExtras() != null) {
                        mPresenter.setSelectedItem(data.getExtras().getParcelable(Constants.SELECTED_ITEM), data.getExtras().getInt(SelectedActivity.FILTER_DATA));
                    }
                }
                break;
        }
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_step1;
    }

    @Override
    public void showDiagnoz(String name) {
        tvDiagnosis.setText(name);
    }

    @Override
    public void showOrganization(String name) {
        tvOrganization.setText(name);
    }
}
