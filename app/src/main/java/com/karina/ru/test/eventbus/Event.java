package com.karina.ru.test.eventbus;

/**
 * Created by Karina on 03.06.2019.
 */
public class Event {

    protected int actionCode;
    protected int mClassUniqueId;
    private EventType mEventType;

    public Event(int actionCode, int mClassUniqueId, EventType eventType) {
        this.actionCode = actionCode;
        this.mClassUniqueId = mClassUniqueId;
        this.mEventType = eventType;
    }

    public int getActionCode() {
        return actionCode;
    }

    public void setActionCode(int actionCode) {
        this.actionCode = actionCode;
    }

    public EventType getmEventType() {
        return mEventType;
    }

    public int getmClassUniqueId() {
        return mClassUniqueId;
    }
}
