package com.karina.ru.test.managers;

import com.karina.ru.test.request.IRequestCallbackNew;
import com.karina.ru.test.rest.response.DataResponse;
import com.karina.ru.test.rest.response.ClientResponse;
import com.karina.ru.test.rest.service.ITestService;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RequestManager {

    private Retrofit mRetrofit;

    @Inject
    public RequestManager(Retrofit retrofit) {
        mRetrofit = retrofit;
    }

    private <S> S createService(Class<S> serviceClass) {
        return mRetrofit.create(serviceClass);
    }

    public <T extends Object> Disposable makeRxSingleRequest(Single<T> single, IRequestCallbackNew requestCallbackNew) {
        return makeSingleNewThreadMainSubscriber(single)
                .doOnSubscribe(disposable -> requestCallbackNew.onStartRequest())
                .doFinally(() -> requestCallbackNew.onFinishRequest())
                .subscribe(t -> requestCallbackNew.onSuccess(t), error -> requestCallbackNew.onErrorRequest(error.getLocalizedMessage()));
    }

    public <T extends Object> Single<T> makeSingleNewThreadMainSubscriber(Single<T> single) {
        return single.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<ClientResponse> getClientResponse(LinkedHashMap<String, String> params) {
        return createService(ITestService.class).getClientResponse(params);
    }

    public Disposable getClientResponse(LinkedHashMap<String, String> params, IRequestCallbackNew requestCallbackNew) {
        return makeRxSingleRequest(getClientResponse(params), requestCallbackNew);
    }

    public Single<ArrayList<DataResponse>> getRegionsResponse() {
        return createService(ITestService.class).getRegionsResponse();
    }

    public Disposable getRegionsResponse(IRequestCallbackNew requestCallbackNew) {
        return makeRxSingleRequest(getRegionsResponse(), requestCallbackNew);
    }

    public Single<ArrayList<DataResponse>> getCitiesResponse(String regionId) {
        return createService(ITestService.class).getCitiesResponse(regionId);
    }

    public Disposable getPolyclinicsResponse(String regionId, IRequestCallbackNew requestCallbackNew) {
        return makeRxSingleRequest(getPolyclinicsResponse(regionId), requestCallbackNew);
    }

    public Single<ArrayList<DataResponse>> getPolyclinicsResponse(String regionId) {
        return createService(ITestService.class).getPolyclinicsResponse(regionId);
    }

    public Disposable getCitiesResponse(String regionId, IRequestCallbackNew requestCallbackNew) {
        return makeRxSingleRequest(getCitiesResponse(regionId), requestCallbackNew);
    }

    public Single<Response<ClientResponse>> createClientResponse(LinkedHashMap<String, String> params) {
        return createService(ITestService.class).createClientResponse(params);
    }

    public Disposable createClientResponse(LinkedHashMap<String, String> params, IRequestCallbackNew requestCallbackNew) {
        return makeRxSingleRequest(createClientResponse(params), requestCallbackNew);
    }
}