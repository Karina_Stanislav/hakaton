package com.karina.ru.test.main.step4.view;

import android.os.Bundle;

import com.karina.ru.test.R;
import com.karina.ru.test.activity.BaseBackContainerActivity;

public class Step4Activity extends BaseBackContainerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.step4_title);
        openFragment();
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.content, getScreenCreator().newInstance(Step4Fragment.class)).commitAllowingStateLoss();
    }
}
