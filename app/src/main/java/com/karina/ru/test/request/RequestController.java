package com.karina.ru.test.request;


import com.karina.ru.test.eventbus.IEventSubject;

public  class RequestController implements IRequestCallbackNew {

    protected IEventSubject mObservable;
    protected int mActionCode;
    protected int mClassUniqueId;

    public RequestController(IEventSubject subject, int mActionCode, int mClassUniqueId){
        this.mObservable = subject;
        this.mActionCode = mActionCode;
        this.mClassUniqueId = mClassUniqueId;
    }


    @Override
    public void onStartRequest() {
        mObservable.notifyStartedWithAction(mActionCode, mClassUniqueId);
    }

    @Override
    public void onFinishRequest() {
        mObservable.notifyFinishWithAction(mActionCode,mClassUniqueId);
    }

    @Override
    public void onErrorRequest(String message) {
        mObservable.notifyFailed(mActionCode,mClassUniqueId,message);
    }

    @Override
    public void onSuccess(Object data) {
        mObservable.notifySuccess(mActionCode,data,mClassUniqueId);
    }
}
