package com.karina.ru.test.dataclases;


import android.os.Parcel;
import android.os.Parcelable;

public class StepTwo implements Parcelable {

    private String appointmentTypeId;

    private String appointmentId;

    private String appointmentDate;

    private String medicTypeId;

    public String getAppointmentTypeId() {
        return appointmentTypeId;
    }

    public void setAppointmentTypeId(String appointmentTypeId) {
        this.appointmentTypeId = appointmentTypeId;
    }

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getMedicTypeId() {
        return medicTypeId;
    }

    public void setMedicTypeId(String medicTypeId) {
        this.medicTypeId = medicTypeId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.appointmentTypeId);
        dest.writeString(this.appointmentId);
        dest.writeString(this.appointmentDate);
        dest.writeString(this.medicTypeId);
    }

    public StepTwo() {
    }

    protected StepTwo(Parcel in) {
        this.appointmentTypeId = in.readString();
        this.appointmentId = in.readString();
        this.appointmentDate = in.readString();
        this.medicTypeId = in.readString();
    }

    public static final Parcelable.Creator<StepTwo> CREATOR = new Parcelable.Creator<StepTwo>() {
        @Override
        public StepTwo createFromParcel(Parcel source) {
            return new StepTwo(source);
        }

        @Override
        public StepTwo[] newArray(int size) {
            return new StepTwo[size];
        }
    };
}
