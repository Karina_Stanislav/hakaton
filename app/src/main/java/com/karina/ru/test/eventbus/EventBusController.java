package com.karina.ru.test.eventbus;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class EventBusController implements IEventSubject {

    private ArrayList<IEventBusObserver> mObservers;

    @Inject
    public EventBusController() {
        mObservers = new ArrayList<>();
    }

    @Override
    public void addObserver(IEventBusObserver iObserver) {
        if (!mObservers.contains(iObserver)) {
            mObservers.add(iObserver);
        }
    }

    @Override
    public void removeObserver(IEventBusObserver iObserver) {
        if (iObserver != null) {
            final int i = mObservers.indexOf(iObserver);
            if (i >= 0) {
                mObservers.remove(iObserver);
            }
        }
    }

    @Override
    public void removeAllObservers() {
        if (mObservers != null) {
            mObservers.clear();
        }
    }


    @Override
    public void notifyEvent(Event event) {
        for (IEventBusObserver observer : mObservers) {
            observer.onEvent(event);
        }
    }

    @Override
    public void notifyStartedWithAction(int action, int classUniqueId) {
        Event event = new Event(action, classUniqueId, EventType.START_REQUEST);
        for (IEventBusObserver observer : mObservers) {
            observer.onStartEvent(event);
        }
    }

    @Override
    public void notifyFinishWithAction(int action, int classUniqueId) {
        Event event = new Event(action, classUniqueId, EventType.FINISH_REQUEST);
        for (IEventBusObserver observer : mObservers) {
            observer.onFinishEvent(event);
        }
    }

    @Override
    public void notifySuccess(int actionCode, Object data, int classUniqueId) {
        notifyEvent(new EventSuccess(actionCode, classUniqueId, data));
    }

    @Override
    public void notifyFailed(int action, int classUniqueId, String message) {
        notifyEvent(new EventFail(action, classUniqueId, message));
    }


}
