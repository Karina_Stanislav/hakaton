package com.karina.ru.test.main.step4.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.karina.ru.test.R;
import com.karina.ru.test.di.components.FragmentComponent;
import com.karina.ru.test.fragment.BaseMvpFragment;
import com.karina.ru.test.main.step4.IStep4Contract;
import com.karina.ru.test.utils.Constants;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class Step4Fragment extends BaseMvpFragment implements IStep4Contract.View {

    @Inject
    IStep4Contract.Presenter mPresenter;

    @BindView(R.id.tv_technical_state)
    TextView tvTechnicalState;
    @BindView(R.id.id_rate_technical_state)
    RatingBar idRateTechnicalState;
    @BindView(R.id.tv_politeness)
    TextView tvPoliteness;
    @BindView(R.id.id_rate_politeness)
    RatingBar idRatePoliteness;
    @BindView(R.id.tv_neatness)
    TextView tvNeatness;
    @BindView(R.id.id_rate_neatness)
    RatingBar idRateNeatness;
    @BindView(R.id.btn_next)
    Button btnNext;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.onCreate(savedInstanceState);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               mPresenter.saveData();
            }
        });
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_step4;
    }

}
