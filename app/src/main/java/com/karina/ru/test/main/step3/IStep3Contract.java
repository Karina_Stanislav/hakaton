package com.karina.ru.test.main.step3;

import com.karina.ru.test.mvp.IBaseMvpPresenter;
import com.karina.ru.test.mvp.IBaseMvpView;
import com.karina.ru.test.rest.response.DataResponse;

public interface IStep3Contract {


    interface View extends IBaseMvpView {


        void showWayToWrite(String name);

        void showTime(String name);
    }

    interface Presenter extends IBaseMvpPresenter<View> {

        void setSelectedItem(DataResponse item, int filterMode);

    }
}
