package com.karina.ru.test.main.signup.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.karina.ru.test.R;
import com.karina.ru.test.di.components.FragmentComponent;
import com.karina.ru.test.fragment.BaseMvpFragment;
import com.karina.ru.test.main.MainActivity;
import com.karina.ru.test.main.selected.view.SelectedActivity;
import com.karina.ru.test.main.signup.ISignUpContract;
import com.karina.ru.test.utils.Constants;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class SignUpFragment extends BaseMvpFragment implements ISignUpContract.View {


    @Inject
    ISignUpContract.Presenter mPresenter;

    @BindView(R.id.btn_sign_up)
    Button btnSignUp;
    @BindView(R.id.et_oms)
    EditText etOms;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.tv_region)
    TextView tvRegion;
    @BindView(R.id.tv_city)
    TextView tvCity;
    @BindView(R.id.tv_organization)
    TextView tvOrganization;
    @BindView(R.id.tv_fio)
    EditText etFio;
    @BindView(R.id.et_login)
    EditText etLogin;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_repeat_password)
    EditText etRepeatPassword;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity.setTitle(R.string.sign_up_title);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_sign_up;
    }

    @OnClick({R.id.btn_sign_up, R.id.tv_region, R.id.tv_city, R.id.tv_organization})
    public void onClickView(View view) {

        switch (view.getId()) {
            case R.id.btn_sign_up:
                mPresenter.signUp(etFio.getText().toString(), etOms.getText().toString(), etEmail.getText().toString(), etLogin.getText().toString(), etPassword.getText().toString(), etRepeatPassword.getText().toString());
                break;
            case R.id.tv_region:
                Bundle bundle = new Bundle();
                bundle.putInt(SelectedActivity.FILTER_DATA, Constants.REGION);
                getScreenCreator().startActivity(this, mActivity, SelectedActivity.class, bundle, Constants.START_SELECTED_ACTIVITY);
                break;
            case R.id.tv_city:
                mPresenter.openSelectedScreen(Constants.CITY);
                break;
            case R.id.tv_organization:
                mPresenter.openSelectedScreen(Constants.ORGANIZATION);
                break;
        }
    }

    @Override
    public void showSelectedScreen(int filterMode, String id) {
        Bundle bundle = new Bundle();
        bundle.putInt(SelectedActivity.FILTER_DATA, filterMode);
        bundle.putString(SelectedActivity.ID, id);
        getScreenCreator().startActivity(this, mActivity, SelectedActivity.class, bundle, Constants.START_SELECTED_ACTIVITY);
    }

    @Override
    public void showCity(String item) {
        tvCity.setText(item);
    }

    @Override
    public void showRegion(String item) {
        tvRegion.setText(item);
    }

    @Override
    public void showOrganization(String item) {
        tvOrganization.setText(item);
    }

    @Override
    public void showMainScreen() {
        ((MainActivity) mActivity).openFragment();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.START_SELECTED_ACTIVITY:
                if (resultCode == Constants.RESULT_SELECTED_ACTIVITY) {
                    if (data.getExtras() != null) {
                        mPresenter.setSelectedItem(data.getExtras().getParcelable(Constants.SELECTED_ITEM), data.getExtras().getInt(SelectedActivity.FILTER_DATA));
                    }
                }
                break;
        }
    }

}
