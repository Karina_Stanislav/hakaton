package com.karina.ru.test.main.step1.presenter;

import android.os.Bundle;

import com.karina.ru.test.eventbus.Event;
import com.karina.ru.test.main.step1.IStep1Contract;
import com.karina.ru.test.mvp.BaseEventBusPresenter;
import com.karina.ru.test.rest.response.DataResponse;
import com.karina.ru.test.utils.Constants;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;

public class Step1Presenter extends BaseEventBusPresenter<IStep1Contract.View>
        implements IStep1Contract.Presenter {

    private Disposable mDisposable;

    @Inject
    public Step1Presenter() {
        mDisposable = Disposables.empty();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {

        }
    }

    @Override
    public void setSelectedItem(DataResponse item, int filterMode) {
        switch (filterMode) {
            case Constants.DIAGNOZ:
                getMvpView().showDiagnoz(item.getName());
                break;
            case Constants.ORGANIZATION:
                getMvpView().showOrganization(item.getName());
                break;
        }
    }


    @Override
    public void onStartEvent(Event event) {

    }

    @Override
    public void onFinishEvent(Event event) {

    }

    @Override
    public void onEvent(Event event) {

    }
}
