package com.karina.ru.test.di.components;

import com.karina.ru.test.di.module.FragmentModule;
import com.karina.ru.test.di.module.PresenterModule;
import com.karina.ru.test.di.scope.PerFragment;
import com.karina.ru.test.main.main.view.MainFragment;
import com.karina.ru.test.main.selected.view.SelectedFragment;
import com.karina.ru.test.main.signin.view.SignInFragment;
import com.karina.ru.test.main.signup.view.SignUpFragment;
import com.karina.ru.test.main.step1.view.Step1Fragment;
import com.karina.ru.test.main.step2.view.Step2Fragment;
import com.karina.ru.test.main.step3.view.Step3Fragment;
import com.karina.ru.test.main.step4.view.Step4Fragment;

import dagger.Subcomponent;

@PerFragment
@Subcomponent(modules = {FragmentModule.class, PresenterModule.class})
public interface FragmentComponent {

    void inject(SignUpFragment signUpFragment);

    void inject(SelectedFragment selectedFragment);

    void inject(SignInFragment signInFragment);

    void inject(MainFragment mainFragment);

    void inject(Step1Fragment step1Fragment);

    void inject(Step2Fragment step2Fragment);

    void inject(Step3Fragment step3Fragment);

    void inject(Step4Fragment step4Fragment);

}
