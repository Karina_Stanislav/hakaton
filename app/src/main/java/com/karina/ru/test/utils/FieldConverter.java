package com.karina.ru.test.utils;

import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.karina.ru.test.App;
import com.karina.ru.test.R;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class FieldConverter {

    public static String getString(int resId) {
        return App.mInstance.getResources().getString(resId);
    }

    public static String[] getStringArray(int resId) {
        return App.mInstance.getResources().getStringArray(resId);
    }

    public static int getColor(int resId) {
        return ContextCompat.getColor(App.mInstance, resId);
    }

    public static int[] getIntArray(int resId) {
        return App.mInstance.getResources().getIntArray(resId);
    }

    public static TypedArray getTypedArray(int resId) {
        return App.mInstance.getResources().obtainTypedArray(resId);
    }

    public static Drawable getDrawable(int resId) {
        return ContextCompat.getDrawable(App.mInstance, resId);
    }

    public static float getDimension(int resId) {
        return App.mInstance.getResources().getDimension(resId);
    }


    public static String formatDateFromTimeStamp(String timeStamp) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+'HH:mm");
        //2017-12-21T00:00:00.000+07:00
        SimpleDateFormat resultDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        try {
            Date date = simpleDateFormat.parse(timeStamp);
            return resultDateFormat.format(date);
        } catch (ParseException e) {
            return null;
        }
    }

}