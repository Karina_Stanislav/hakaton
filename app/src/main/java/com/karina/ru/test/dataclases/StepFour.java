package com.karina.ru.test.dataclases;

import android.os.Parcel;
import android.os.Parcelable;

public class StepFour implements Parcelable {

    private int technicalState;

    private int politeness;

    private int neatness;

    public int getTechnicalState() {
        return technicalState;
    }

    public void setTechnicalState(int technicalState) {
        this.technicalState = technicalState;
    }

    public int getPoliteness() {
        return politeness;
    }

    public void setPoliteness(int politeness) {
        this.politeness = politeness;
    }

    public int getNeatness() {
        return neatness;
    }

    public void setNeatness(int neatness) {
        this.neatness = neatness;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.technicalState);
        dest.writeInt(this.politeness);
        dest.writeInt(this.neatness);
    }

    public StepFour() {
    }

    protected StepFour(Parcel in) {
        this.technicalState = in.readInt();
        this.politeness = in.readInt();
        this.neatness = in.readInt();
    }

    public static final Parcelable.Creator<StepFour> CREATOR = new Parcelable.Creator<StepFour>() {
        @Override
        public StepFour createFromParcel(Parcel source) {
            return new StepFour(source);
        }

        @Override
        public StepFour[] newArray(int size) {
            return new StepFour[size];
        }
    };
}
