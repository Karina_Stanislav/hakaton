package com.karina.ru.test.di.module;

import android.app.Service;
import android.content.Context;

import com.karina.ru.test.di.qualifier.ServiceContext;
import com.karina.ru.test.di.scope.PerService;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModule {

    private final Service mService;

    public ServiceModule(Service service) {
        this.mService = service;
    }

    @Provides
    @PerService
    Service provideService() {
        return mService;
    }

    @Provides
    @PerService
    @ServiceContext
    Context provideContext() {
        return mService.getApplicationContext();
    }

}
