package com.karina.ru.test.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.karina.ru.test.activity.BaseActivity;
import com.karina.ru.test.screencreator.ScreenCreator;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment {

    private Unbinder mUnbinder;

    protected Activity mActivity;

    protected boolean isSavedInstanceStateDone;
    protected boolean isMakeStartRequest;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            mActivity = (Activity) context;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof BaseActivity) {
            mActivity = activity;
        }
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    protected ScreenCreator getScreenCreator() {
        return ScreenCreator.getInstance();
    }


    protected abstract int getLayout();

    protected void retry() {
    }
//
//    @Override
//    public void unAuthorized() {
//        getActivity().setResult(Constants.RESULT_LOGOUT);
//        getActivity().finish();
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 0) {
//            unAuthorized();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        isSavedInstanceStateDone = false;
        /*if(TextUtils.isEmpty(App.get(mActivity).getComponent().dataManager().getSessionId())){
            unAuthorized();
        }*/
    }

//    protected abstract void checkSessionState();

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        isSavedInstanceStateDone = true;
    }

    public boolean isSavedInstanceStateDone() {
        return isSavedInstanceStateDone;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        mActivity = null;
    }



}