package com.karina.ru.test.main.step2.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.karina.ru.test.R;
import com.karina.ru.test.di.components.FragmentComponent;
import com.karina.ru.test.fragment.BaseMvpFragment;
import com.karina.ru.test.main.selected.view.SelectedActivity;
import com.karina.ru.test.main.step1.view.Step1Fragment;
import com.karina.ru.test.main.step2.IStep2Contract;
import com.karina.ru.test.main.step3.view.Step3Activity;
import com.karina.ru.test.utils.Constants;

import javax.inject.Inject;

import butterknife.BindView;

public class Step2Fragment extends BaseMvpFragment implements IStep2Contract.View {

    @Inject
    IStep2Contract.Presenter mPresenter;

    @BindView(R.id.tv_way_to_write)
    TextView tvWayToWrite;
    @BindView(R.id.et_ticket_number)
    EditText etTicketNumber;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_specialist)
    TextView tvSpecialist;
    @BindView(R.id.btn_next)
    Button btnNext;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.onCreate(savedInstanceState);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getScreenCreator().startActivity(mActivity, Step3Activity.class, Constants.START_STEP1_ACTIVITY);
            }
        });
        tvWayToWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(SelectedActivity.FILTER_DATA, Constants.WayToWrite);
                getScreenCreator().startActivity(Step2Fragment.this, mActivity, SelectedActivity.class, bundle, Constants.START_SELECTED_ACTIVITY);
            }
        });
        tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(SelectedActivity.FILTER_DATA, Constants.Time);
                getScreenCreator().startActivity(Step2Fragment.this, mActivity, SelectedActivity.class, bundle, Constants.START_SELECTED_ACTIVITY);
            }
        });
        tvSpecialist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(SelectedActivity.FILTER_DATA, Constants.Specialist);
                getScreenCreator().startActivity(Step2Fragment.this, mActivity, SelectedActivity.class, bundle, Constants.START_SELECTED_ACTIVITY);
            }
        });
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_step2;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.START_SELECTED_ACTIVITY:
                if (resultCode == Constants.RESULT_SELECTED_ACTIVITY) {
                    if (data.getExtras() != null) {
                        mPresenter.setSelectedItem(data.getExtras().getParcelable(Constants.SELECTED_ITEM), data.getExtras().getInt(SelectedActivity.FILTER_DATA));
                    }
                }
                break;
        }
    }

    @Override
    public void showWayToWrite(String name) {
        tvWayToWrite.setText(name);
    }

    @Override
    public void showTime(String name) {
        tvTime.setText(name);
    }

    @Override
    public void showSpecialist(String name) {
        tvSpecialist.setText(name);
    }

}
