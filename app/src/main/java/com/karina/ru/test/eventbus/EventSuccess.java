package com.karina.ru.test.eventbus;

/**
 * Created by Karina on 03.06.2019.
 */
public class EventSuccess extends Event {

    private Object data;

    public EventSuccess(int actionCode, int mClassUniqueId, Object data) {
        super(actionCode, mClassUniqueId, EventType.SUCCESS_REQUEST);
        this.data = data;
    }

    public Object getData() {
        return data;
    }
}
