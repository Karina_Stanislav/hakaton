package com.karina.ru.test.request;

public class Pair<T> {

    private T mValue;

    public Pair(T value) {
        this.mValue = value;
    }

    public Object getValue() {
        return mValue;
    }

    public void setValue(T value) {
        this.mValue = value;
    }
}
