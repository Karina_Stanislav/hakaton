package com.karina.ru.test.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.karina.ru.test.di.qualifier.ActivityContext;
import com.karina.ru.test.di.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        mActivity = activity;
    }

    @Provides
    @PerActivity
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    @PerActivity
    @ActivityContext
    Context providesContext() {
        return mActivity;
    }

}
