package com.karina.ru.test.main.selected.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.karina.ru.test.R;
import com.karina.ru.test.rest.response.DataResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karina on 29.05.2019.
 */
public class SelectedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DataResponse> dataList = new ArrayList<>();

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public void setDataList(List<DataResponse> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder = null;
        return new ViewHolderItem(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_list, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ViewHolderItem viewHolderItem = (ViewHolderItem) viewHolder;
        viewHolderItem.tvItem.setText(dataList.get(i).getName());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public class ViewHolderItem extends RecyclerView.ViewHolder {

        TextView tvItem;

        public ViewHolderItem(@NonNull View itemView) {
            super(itemView);
            tvItem = itemView.findViewById(R.id.tv_item);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(getAdapterPosition());
                    }
                }
            });
        }
    }
}
