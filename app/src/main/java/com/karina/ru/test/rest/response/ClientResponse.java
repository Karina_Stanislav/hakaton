package com.karina.ru.test.rest.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ClientResponse implements Parcelable {

    @SerializedName("id")
    private long clientId;
    @SerializedName("fullNaMe")
    private String fullNaMe;
    @SerializedName("oms")
    private String oms;

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public String getFullNaMe() {
        return fullNaMe;
    }

    public void setFullNaMe(String fullNaMe) {
        this.fullNaMe = fullNaMe;
    }

    public String getOms() {
        return oms;
    }

    public void setOms(String oms) {
        this.oms = oms;
    }

    public ClientResponse() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.clientId);
        dest.writeString(this.fullNaMe);
        dest.writeString(this.oms);
    }

    protected ClientResponse(Parcel in) {
        this.clientId = in.readLong();
        this.fullNaMe = in.readString();
        this.oms = in.readString();
    }

    public static final Creator<ClientResponse> CREATOR = new Creator<ClientResponse>() {
        @Override
        public ClientResponse createFromParcel(Parcel source) {
            return new ClientResponse(source);
        }

        @Override
        public ClientResponse[] newArray(int size) {
            return new ClientResponse[size];
        }
    };
}
