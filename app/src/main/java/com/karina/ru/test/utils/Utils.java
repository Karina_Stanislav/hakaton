package com.karina.ru.test.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    private static final int PHONE_NUMBER_LENGTH = 10;

    public static String getStatusDateFormat(Date chosenDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy, HH:mm");
        return simpleDateFormat.format(chosenDate);
    }

    public static boolean isNotEmptyField(String text) {
        return !text.trim().isEmpty();
    }


    public static Boolean equalsDate(String timeFirst, String timeSecond, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        SimpleDateFormat resultDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        try {
            Date dateFirst = simpleDateFormat.parse(timeFirst);
            Date dateSecond = simpleDateFormat.parse(timeSecond);
            if (resultDateFormat.format(dateFirst).equalsIgnoreCase(resultDateFormat.format(dateSecond))) {
                return true;
            } else {
                return false;
            }
        } catch (ParseException e) {
            return false;
        }
    }

    public static String formatDateWithTime(String timeStamp) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat resultDateFormat = new SimpleDateFormat("dd.MM.yyyy, HH:mm");
        try {
            Date date = simpleDateFormat.parse(timeStamp);

            return resultDateFormat.format(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static boolean isValidPhone(String phoneNumber) {
        return phoneNumber != null
                && !phoneNumber.isEmpty()
                && phoneNumber.length() == PHONE_NUMBER_LENGTH;
    }

}
