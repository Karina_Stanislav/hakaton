package com.karina.ru.test.main.signup.presenter;

import android.os.Bundle;

import com.karina.ru.test.Actions;
import com.karina.ru.test.eventbus.Event;
import com.karina.ru.test.eventbus.EventSuccess;
import com.karina.ru.test.main.signup.ISignUpContract;
import com.karina.ru.test.mvp.BaseEventBusPresenter;
import com.karina.ru.test.rest.RequestParams;
import com.karina.ru.test.rest.response.ClientResponse;
import com.karina.ru.test.rest.response.DataResponse;
import com.karina.ru.test.utils.Constants;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;

public class SignUpPresenter extends BaseEventBusPresenter<ISignUpContract.View> implements ISignUpContract.Presenter {

    private Disposable mDisposable;

    private String idRegion;
    private String idCity;
    private String idOrganization;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Inject
    public SignUpPresenter() {
        mDisposable = Disposables.empty();
    }


    @Override
    public void setSelectedItem(DataResponse item, int filterMode) {
        switch (filterMode) {
            case Constants.REGION:
                idRegion = String.valueOf(item.getId());
                getMvpView().showRegion(item.getName());
                break;
            case Constants.CITY:
                idCity = String.valueOf(item.getId());
                getMvpView().showCity(item.getName());
                break;
            case Constants.ORGANIZATION:
                idOrganization = String.valueOf(item.getId());
                getMvpView().showOrganization(item.getName());
                break;
        }
    }

    @Override
    public void openSelectedScreen(int filterMode) {
        switch (filterMode) {
            case Constants.CITY:
                getMvpView().showSelectedScreen(filterMode, idRegion);
                break;
            case Constants.ORGANIZATION:
                getMvpView().showSelectedScreen(filterMode, idCity);
                break;
        }
    }

    @Override
    public void signUp(String fio, String oms, String email, String login, String password, String repeatPassword) {
        if(password.equalsIgnoreCase(repeatPassword)) {
            mDisposable = getRequestManager().createClientResponse(RequestParams.createClientParams(fio, oms, idOrganization, login, password, email), getRequestController(Actions.CREATE_CLIENT));
            addDisposable(mDisposable);
        }
    }

    @Override
    public void onStartEvent(Event event) {

    }

    @Override
    public void onFinishEvent(Event event) {

    }

    @Override
    public void onEvent(Event event) {
        if (event.getmClassUniqueId() == getMvpView().getClassUniqueDeviceId()) {
            switch (event.getmEventType()) {
                case SUCCESS_REQUEST:
                    switch (event.getActionCode()) {
                        case Actions.GET_REGIONS:
                        case Actions.GET_CITIES:
                        case Actions.GET_POLYCLINICS:
                            ClientResponse dataResponse = (ClientResponse) ((EventSuccess) event).getData();
                            getMvpView().showMainScreen();
                            break;
                    }
                    break;
            }

        }
    }
}
