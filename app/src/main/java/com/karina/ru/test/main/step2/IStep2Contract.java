package com.karina.ru.test.main.step2;

import com.karina.ru.test.mvp.IBaseMvpPresenter;
import com.karina.ru.test.mvp.IBaseMvpView;
import com.karina.ru.test.rest.response.DataResponse;

public interface IStep2Contract {


    interface View extends IBaseMvpView {


        void showWayToWrite(String name);

        void showTime(String name);

        void showSpecialist(String name);
    }

    interface Presenter extends IBaseMvpPresenter<View> {

        void setSelectedItem(DataResponse item, int filterMode);
    }
}
