package com.karina.ru.test.main.step4.presenter;

import android.os.Bundle;

import com.karina.ru.test.eventbus.Event;
import com.karina.ru.test.main.step4.IStep4Contract;
import com.karina.ru.test.mvp.BaseEventBusPresenter;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;

public class Step4Presenter extends BaseEventBusPresenter<IStep4Contract.View>
        implements IStep4Contract.Presenter {

    private Disposable mDisposable;

    @Inject
    public Step4Presenter() {
        mDisposable = Disposables.empty();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {

        }
    }

    @Override
    public void onStartEvent(Event event) {

    }

    @Override
    public void onFinishEvent(Event event) {

    }

    @Override
    public void onEvent(Event event) {

    }

    @Override
    public void saveData() {

    }
}
