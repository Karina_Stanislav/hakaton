package com.karina.ru.test.di.module;

import android.content.Context;
import android.view.View;

import com.karina.ru.test.di.scope.PerView;

import dagger.Module;
import dagger.Provides;

@Module
public class ViewModule {

    private final View mView;

    public ViewModule(View view){
        this.mView = view;
    }

    @Provides
    @PerView
    View provideView(){
        return mView;
    }

    @Provides
    @PerView
    Context provideContext(){
        return mView.getContext();
    }

}
